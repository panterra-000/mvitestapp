package mapp.test.mvitestapp.viewmodels

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import mapp.test.mvitestapp.base.BaseViewModel
import mapp.test.mvitestapp.base.Reducer
import mapp.test.mvitestapp.data.MainScreenState
import mapp.test.mvitestapp.data.MainScreenUiEvent
import mapp.test.mvitestapp.data.MainSideEffect
import mapp.test.mvitestapp.data.MyItem
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
) : BaseViewModel<MainScreenState, MainScreenUiEvent, MainSideEffect>() {

    private val repository = FakeRepository

    private val reducer = MainReducer(MainScreenState.initial())

    override val state: StateFlow<MainScreenState>
        get() = reducer.state

    override val sideEffect: Flow<MainSideEffect>
        get() = reducer.sideEffect

    init {
        viewModelScope.launch {
            val data = repository.getData()
            sendEvent(MainScreenUiEvent.ShowData(data))
        }
    }

    fun sendEvent(event: MainScreenUiEvent) {
        reducer.sendEvent(event)
    }

    private class MainReducer(initial: MainScreenState) : Reducer<MainScreenState, MainScreenUiEvent, MainSideEffect>(initial) {

        override fun reduce(oldState: MainScreenState, event: MainScreenUiEvent) {
            when (event) {
                is MainScreenUiEvent.ShowData -> {
                    setState(oldState.copy(isShowProgress = false, data = event.items, error = null))
                }

                is MainScreenUiEvent.NavigateTo -> {
                    setSideEffect(MainSideEffect.NavigateTo(event.route))
                }
            }
        }
    }
}

object FakeRepository {
    suspend fun getData(): List<MyItem> {
        delay(1500)
        return listOf(
            MyItem("1", "Name1"),
            MyItem("2", "Name2"),
            MyItem("3", "Name3"),
            MyItem("4", "Name4"),
            MyItem("5", "Name5"),
        )
    }
}