package mapp.test.mvitestapp.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.Flow

abstract class BaseViewModel<T : UiState, in E : UiEvent, SE : UISideEffect> : ViewModel() {

    abstract val state: Flow<T>
    abstract val sideEffect: Flow<SE>

}

interface UiState

interface UiEvent

interface UISideEffect