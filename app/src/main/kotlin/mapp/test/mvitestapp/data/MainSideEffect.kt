package mapp.test.mvitestapp.data

import mapp.test.mvitestapp.base.UISideEffect

sealed class MainSideEffect : UISideEffect {
    data class ShowError(val message: String) : MainSideEffect()
    data class NavigateTo(val route: String) : MainSideEffect()
}