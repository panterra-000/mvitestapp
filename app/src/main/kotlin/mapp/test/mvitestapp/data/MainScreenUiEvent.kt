package mapp.test.mvitestapp.data

import androidx.compose.runtime.Immutable
import mapp.test.mvitestapp.base.UiEvent

@Immutable
sealed class MainScreenUiEvent : UiEvent {
    data class ShowData(val items: List<MyItem>) : MainScreenUiEvent()
    data class NavigateTo(val route: String) : MainScreenUiEvent()
}



