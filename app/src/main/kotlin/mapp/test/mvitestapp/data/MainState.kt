package mapp.test.mvitestapp.data

import androidx.compose.runtime.Immutable
import mapp.test.mvitestapp.base.UiState

@Immutable
data class MainScreenState(
    val isShowProgress: Boolean = false,
    val error: Throwable? = null,
    val data: List<MyItem>
) : UiState {
    companion object {
        fun initial() = MainScreenState(
            isShowProgress = true,
            data = emptyList(),
            error = null
        )
    }

    override fun toString(): String {
        return "isLoading: $isShowProgress, data.size: ${data.size}, error: $error"
    }
}

data class MyItem(
    val id: String,
    val fullName: String
)

