package mapp.test.mvitestapp.ui.screens

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import mapp.test.coreui.compose.base.columns.ColumnMaxSize
import mapp.test.coreui.compose.base.texts.Text24spBoldSecond

@Composable
fun SecondScreen(navController: NavHostController) {
    ColumnMaxSize {
        Text24spBoldSecond(text = "Second Screen")
    }
}