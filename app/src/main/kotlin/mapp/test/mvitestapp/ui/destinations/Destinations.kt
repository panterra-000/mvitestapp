package mapp.test.mvitestapp.ui.destinations

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import mapp.test.mvitestapp.ui.screens.MainScreen
import mapp.test.mvitestapp.ui.screens.SecondScreen
import mapp.test.mvitestapp.utils.MAIN_SCREEN_ROUTE
import mapp.test.mvitestapp.utils.SECOND_SCREEN_ROUTE

fun NavGraphBuilder.mainDestination(
    navController: NavHostController,
) {
    composable(MAIN_SCREEN_ROUTE) {
        MainScreen(navController = navController)
    }
}

fun NavGraphBuilder.secondDestination(
    navController: NavHostController,
) {
    composable(SECOND_SCREEN_ROUTE) {
        SecondScreen(navController = navController)
    }
}



