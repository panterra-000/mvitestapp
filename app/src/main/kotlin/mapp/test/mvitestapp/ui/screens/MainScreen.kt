package mapp.test.mvitestapp.ui.screens

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.graphics.Color
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import mapp.test.coreui.compose.base.ProgressBarWithBlock
import mapp.test.coreui.compose.base.buttons.ButtonPrimaryRounded
import mapp.test.coreui.compose.base.columns.ColumnMaxSizeScrollable
import mapp.test.coreui.compose.base.texts.Text24spBoldSecond
import mapp.test.coreui.compose.base.texts.Text24spTestResult
import mapp.test.mvitestapp.data.MainScreenUiEvent
import mapp.test.mvitestapp.data.MainSideEffect
import mapp.test.mvitestapp.data.MyItem
import mapp.test.mvitestapp.utils.SECOND_SCREEN_ROUTE
import mapp.test.mvitestapp.viewmodels.MainViewModel

@Composable
fun MainScreen(navController: NavHostController, viewModel: MainViewModel = hiltViewModel()) {

    val state by viewModel.state.collectAsState()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.sideEffect.collect {
            when (it) {
                is MainSideEffect.NavigateTo -> {
                    navController.navigate(it.route)
                }

                is MainSideEffect.ShowError -> {}
            }
        }
    })

    ColumnMaxSizeScrollable {
        when {
            state.isShowProgress -> {
                ProgressBarWithBlock()
            }

            state.data.isNotEmpty() -> {
                MainView(list = state.data)
            }

            state.error != null -> {
                ErrorView(throwable = state.error!!)
            }
        }

        ButtonPrimaryRounded(text = "Navigate next") {
            viewModel.sendEvent(MainScreenUiEvent.NavigateTo(SECOND_SCREEN_ROUTE))
        }

    }
}

@Composable
fun ErrorView(throwable: Throwable) {
    Text24spTestResult(text = "Error: $throwable", color = Color.Red)
}

@Composable
fun MainView(list: List<MyItem>) {
    list.forEach {
        Text24spBoldSecond(text = it.fullName)
    }
}
