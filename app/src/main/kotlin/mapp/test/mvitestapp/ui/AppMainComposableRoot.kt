package mapp.test.mvitestapp.ui

import android.annotation.SuppressLint
import androidx.compose.material.Scaffold
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import mapp.test.coreui.theme.PrimaryAppTheme
import mapp.test.mvitestapp.ui.destinations.mainDestination
import mapp.test.mvitestapp.ui.destinations.secondDestination
import mapp.test.mvitestapp.utils.MAIN_SCREEN_ROUTE

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun AppMainComposableRoot() {
    val navController: NavHostController = rememberNavController()
    val scaffoldState = rememberScaffoldState()

    PrimaryAppTheme {
        Scaffold(
            scaffoldState = scaffoldState,
        ) {
            NavHost(
                navController = navController,
                startDestination = MAIN_SCREEN_ROUTE
            ) {
                mainDestination(navController)
                secondDestination(navController)
            }
        }
    }
}
