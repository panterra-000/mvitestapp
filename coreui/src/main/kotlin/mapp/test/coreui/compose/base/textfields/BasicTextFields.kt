package mapp.test.coreui.compose.base.textfields

import androidx.annotation.DrawableRes
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import mapp.test.coreui.theme.TestAppTheme
import mapp.test.coreui.compose.base.Spacer10dp
import mapp.test.coreui.compose.base.Spacer15dp
import mapp.test.coreui.compose.base.texts.Text16Hint


@Composable
fun AppBasicTextField(
    textState: MutableState<String>,
    hintText: String = "",
    @DrawableRes iconId: Int? = null,
    defMarginTop: Boolean = true,
    defMarginBottom: Boolean = false,
) {
    Column {
        if (defMarginTop) Spacer15dp()
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(12.dp)),
            value = textState.value,
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = TestAppTheme
                    .colors.primaryInputBackground,
                cursorColor = Color.Black,
                disabledLabelColor = Color.Transparent,
                focusedIndicatorColor = TestAppTheme
                    .colors.transparent,
                unfocusedIndicatorColor = TestAppTheme
                    .colors.transparent
            ),
            textStyle = TextStyle(
                color = TestAppTheme
                    .colors.primaryText, fontSize = 16.sp
            ),
            onValueChange = {
                textState.value = it
            },
            singleLine = true,
            placeholder = {
                Text16Hint(
                    hintText,
                    color = TestAppTheme
                        .colors.inactiveText
                )
            },
            trailingIcon = {
                if (iconId != null)
                    IconButton(onClick = { }) {
                        Icon(painter = painterResource(id = iconId), contentDescription = null)
                    }
            }
        )
        if (defMarginBottom) Spacer10dp()
    }
}

