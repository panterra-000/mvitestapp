package mapp.test.coreui.compose.base.boxes

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import mapp.test.coreui.theme.TestAppTheme

@Composable
fun BoxPrimaryPadding24Dp(
    content: @Composable BoxScope.() -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(TestAppTheme.colors.primaryBackground)
            .padding(24.dp)
    ) {
        content()
    }
}

@Composable
fun BoxPrimaryHorizontalPadding24Dp(
    content: @Composable BoxScope.() -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(TestAppTheme.colors.primaryBackground)
            .padding(horizontal = 24.dp)
    ) {
        content()
    }
}

@Composable
fun BoxPrimary(
    backgroundColor: Color = TestAppTheme.colors.primaryBackground,
    content: @Composable BoxScope.() -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor)
    ) {
        content()
    }
}

@Composable
fun TransparentBoxWithPadding(
    horizontalPadding: Dp = 16.dp,
    verticalPadding: Dp = 0.dp,
    content: @Composable BoxScope.() -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(Color.Transparent)
            .padding(horizontal = horizontalPadding, vertical = verticalPadding)
    ) {
        content()
    }
}

@Composable
fun BoxMaxsizeAlignCenter(
    content: @Composable BoxScope.() -> Unit
) {
    Box(
        Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        content()
    }
}
