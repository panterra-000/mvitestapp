package mapp.test.coreui.compose.base.date_picker


import android.app.DatePickerDialog
import android.widget.DatePicker
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import mapp.test.coreui.theme.TestAppTheme
import mapp.test.coreui.compose.base.Spacer15dp
import mapp.test.coreui.compose.base.texts.Text12Hint
import mapp.test.coreui.compose.base.texts.Text16Hint
import mapp.test.coreui.compose.base.texts.Text16spBoldTitle
import java.util.*

@Composable
fun AppDatePicker(
    hintText: String = "",
    selectedDate: (String) -> Unit,
    iconResourceId: Int,
    selectedDateForRequest: (String) -> Unit
) {
    val mContext = LocalContext.current

    val mYear: Int
    val mMonth: Int
    val mDay: Int

    val mCalendar = Calendar.getInstance()

    mYear = mCalendar.get(Calendar.YEAR)
    mMonth = mCalendar.get(Calendar.MONTH)
    mDay = mCalendar.get(Calendar.DAY_OF_MONTH)

    mCalendar.time = Date()

    val mDate = remember { mutableStateOf("") }
    val mDateForRequest = remember { mutableStateOf("") }

    val mDatePickerDialog = DatePickerDialog(
        mContext,
        { _: DatePicker, mYear: Int, mMonth: Int, mDayOfMonth: Int ->

            val fDay = if (mDayOfMonth > 9) {
                mDay
            } else {
                "0$mDayOfMonth"
            }
            val fMonth = if ((mMonth + 1) > 9) {
                mMonth + 1
            } else {
                "0${mMonth + 1}"
            }

            mDate.value = "$fDay.$fMonth.$mYear"
            mDateForRequest.value = "$mYear-${mMonth + 1}-$mDayOfMonth"
            selectedDate(mDate.value)
            selectedDateForRequest(mDateForRequest.value)
        }, mYear, mMonth, mDay
    )

    Spacer15dp()
    Row(modifier = Modifier
        .clip(RoundedCornerShape(12.dp))
        .fillMaxWidth()
        .background(TestAppTheme.colors.primaryInputBackground)
        .clickable { mDatePickerDialog.show() }
        .padding(horizontal = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        if (mDate.value.isNotEmpty()) {
            Column(Modifier.padding(vertical = 8.dp)) {
                Text12Hint(text = hintText)
                Text16spBoldTitle(text = mDate.value)
            }
        } else {
            Column(Modifier.padding(vertical = 16.dp)) {
                Text16Hint(text = hintText)
            }
        }
        Spacer(modifier = Modifier.weight(1f))
        Icon(
            painter = painterResource(id = iconResourceId),
            contentDescription = null,
            Modifier
                .height(21.5.dp)
                .width(19.5.dp)
        )
    }
}


@Composable
fun AppDoBPicker(
    hintText: String = "",
    selectedDate: (String) -> Unit,
    selectedDateForRequest: (String) -> Unit,
    iconResourceId: Int,
    backGroundColor: Color = TestAppTheme.colors.primaryInputBackground

) {
    val mContext = LocalContext.current

    val mYear: Int
    val mMonth: Int
    val mDay: Int

    val mCalendar = Calendar.getInstance()

    mYear = mCalendar.get(Calendar.YEAR) - 18
    mMonth = mCalendar.get(Calendar.MONTH)
    mDay = mCalendar.get(Calendar.DAY_OF_MONTH)

    mCalendar.set(Calendar.YEAR, mYear)

    val minDateCalendar = Calendar.getInstance()
    val mMinYear = minDateCalendar.get(Calendar.YEAR) - 100
    minDateCalendar.set(Calendar.YEAR, mMinYear)

    val mDate = remember { mutableStateOf("") }
    val mDateForRequest = remember { mutableStateOf("") }

    val mDatePickerDialog = DatePickerDialog(
        mContext,
        { _: DatePicker, mYear: Int, mMonth: Int, mDayOfMonth: Int ->

            val fDay = if (mDayOfMonth > 9) {
                mDay
            } else {
                "0$mDayOfMonth"
            }
            val fMonth = if ((mMonth + 1) > 9) {
                mMonth + 1
            } else {
                "0${mMonth + 1}"
            }

            mDate.value = "$fDay.$fMonth.$mYear"
            mDateForRequest.value = "$mYear-${mMonth + 1}-$mDayOfMonth"
            selectedDate(mDate.value)
            selectedDateForRequest(mDateForRequest.value)

        }, mYear, mMonth, mDay
    )

    mDatePickerDialog.datePicker.maxDate = mCalendar.timeInMillis
    mDatePickerDialog.datePicker.minDate = minDateCalendar.timeInMillis

    Spacer15dp()
    Row(modifier = Modifier
        .clip(RoundedCornerShape(12.dp))
        .fillMaxWidth()
        .background(backGroundColor)
        .clickable { mDatePickerDialog.show() }
        .padding(horizontal = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        if (mDate.value.isNotEmpty()) {
            Column(Modifier.padding(vertical = 8.dp)) {
                Text12Hint(text = hintText)
                Text16spBoldTitle(text = mDate.value)
            }
        } else {
            Column(Modifier.padding(vertical = 16.dp)) {
                Text16Hint(text = hintText)
            }
        }
        Spacer(modifier = Modifier.weight(1f))
        Icon(
            painter = painterResource(id = iconResourceId),
            contentDescription = null,
            Modifier
                .height(21.5.dp)
                .width(19.5.dp)
        )
    }
}
