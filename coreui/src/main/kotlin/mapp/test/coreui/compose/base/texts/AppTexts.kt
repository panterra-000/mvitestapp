package mapp.test.coreui.compose.base.texts

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import mapp.test.coreui.theme.TestAppTheme


@Composable
fun Text16Inactive(
    text: String,
    alignCenter: Boolean = false,
    color: Color = TestAppTheme.colors.inactiveText
) {
    Text(
        text = text,
        fontSize = 16.sp,
        color = color,
        textAlign = if (alignCenter) TextAlign.Center else TextAlign.Start
    )
}

@Composable
fun Text16InactiveWithWidth(
    text: String,
    color: Color = TestAppTheme.colors.inactiveText,
    width: Dp
) {
    Text(
        text = text,
        fontSize = 16.sp,
        fontWeight = FontWeight.Bold,
        color = color,
        modifier = Modifier.width(width)
    )
}


@Composable
fun Text14NonAction(text: String, color: Color = TestAppTheme.colors.nonActionText) {
    Text(
        text = text,
        fontSize = 14.sp,
        fontWeight = FontWeight.Bold,
        color = color,
    )
}


@Composable
fun Text14Inactive(text: String, color: Color = TestAppTheme.colors.inactiveText) {
    Text(
        text = text,
        fontSize = 14.sp,
        color = color,
    )
}

@Composable
fun Text14InactiveCenter(text: String, color: Color = TestAppTheme.colors.inactiveText) {
    Text(
        text = text,
        fontSize = 14.sp,
        color = color,
        textAlign = TextAlign.Center,
        modifier = Modifier.fillMaxWidth()
    )
}


@Composable
fun Text14CardInactiveTitle(
    text: String,
    color: Color = TestAppTheme.colors.cardItemInactiveTitleText
) {
    Text(
        text = text,
        fontSize = 14.sp,
        color = color,
    )
}

@Composable
fun Text16Hint(text: String, color: Color = TestAppTheme.colors.inactiveText) {
    Text(
        text = text,
        fontSize = 16.sp,
        color = color,
    )
}


@Composable
fun Text12Hint(
    text: String,
    alignCenter: Boolean = false,
    color: Color = TestAppTheme.colors.inactiveText
) {
    Text(
        text = text,
        fontSize = 12.sp,
        color = color,
        textAlign = if (alignCenter) TextAlign.Center else TextAlign.Start
    )
}


@Composable
fun Text12Disable(
    text: String,
    alignCenter: Boolean = false,
    color: Color = TestAppTheme.colors.disableText
) {
    Text(
        text = text,
        fontSize = 12.sp,
        color = color,
        textAlign = if (alignCenter) TextAlign.Center else TextAlign.Start
    )
}


@Composable
fun Text16spTitle(
    text: String,
    color: Color = TestAppTheme.colors.primaryText,
    alignCenter: Boolean = true
) {
    Text(
        text = text,
        fontSize = 16.sp,
        color = color,
        textAlign = if (alignCenter) TextAlign.Center else TextAlign.Start
    )
}


@Composable
fun Text14spTitleCenter(
    text: String,
    color: Color = TestAppTheme.colors.primaryText,
    alignCenter: Boolean = true
) {
    Text(
        text = text,
        fontSize = 14.sp,
        color = color,
        textAlign = if (alignCenter) TextAlign.Center else TextAlign.Start
    )
}

@Composable
fun Text16spWithSelected(
    text: String,
    selected: Boolean = false,
) {
    Text(
        text = text,
        fontSize = 16.sp,
        color = if (selected) TestAppTheme.colors.primaryButtonText else TestAppTheme.colors.primaryText,
    )
}


@Composable
fun Text12spWithBackground(text: String, color: Color = TestAppTheme.colors.primaryText) {
    Text(
        text = text,
        fontSize = 12.sp,
        color = color,
        modifier = Modifier
            .padding(12.dp)
            .background(TestAppTheme.colors.transparentResult)
            .padding(6.dp)
    )
}


@Composable
fun Text12Inactive(text: String, color: Color = TestAppTheme.colors.inactiveText) {
    Text(
        text = text,
        fontSize = 12.sp,
        color = color,
    )
}


@Composable
fun ErrorText12sp(text: String, color: Color = TestAppTheme.colors.errorText) {
    Text(
        modifier = Modifier.padding(start = 16.dp),
        text = text,
        fontSize = 12.sp,
        color = color,
    )
}


@Composable
fun ActiveText12spTextAlignEnd(text: String, color: Color = TestAppTheme.colors.errorText) {
    Text(
        modifier = Modifier
            .fillMaxWidth()
            .padding(end = 12.dp),
        text = text,
        textAlign = TextAlign.End,
        fontSize = 12.sp,
        color = color,
    )
}


@Composable
fun RecommendText12sp(text: String, color: Color = TestAppTheme.colors.inactiveText) {
    Text(
        modifier = Modifier.padding(start = 16.dp),
        text = text,
        fontSize = 12.sp,
        color = color,
    )
}

@Composable
fun Text12EllipsizeInactive(text: String, color: Color = TestAppTheme.colors.inactiveText) {
    Text(
        text = text,
        fontSize = 12.sp,
        color = color,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis
    )
}