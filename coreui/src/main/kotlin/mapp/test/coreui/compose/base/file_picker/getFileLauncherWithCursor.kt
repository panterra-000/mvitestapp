package mapp.test.coreui.compose.base.file_picker

import android.net.Uri
import android.provider.OpenableColumns
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import java.io.File
import java.io.FileOutputStream

@Composable
fun getFileLauncherWithCursor(
    fileCall: (File, Uri, String, String?) -> Unit,
    onWrongFormat: () -> Unit
): ManagedActivityResultLauncher<String, Uri?> {
    val context = LocalContext.current
    val contentResolver = context.contentResolver
    val cacheDir = context.cacheDir
    return rememberLauncherForActivityResult(contract = ActivityResultContracts.GetContent()) { uri: Uri? ->
        uri?.let {
            var logoName = "logo.png"
            context.contentResolver.query(uri, null, null, null, null)?.use { cursor ->
                val nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                cursor.moveToFirst()
                logoName = cursor.getString(nameIndex)
                if (logoName.endsWith(".png") || logoName.endsWith(".jpg") ||
                    logoName.endsWith(".jpeg") || logoName.endsWith(".pdf") ||
                    logoName.endsWith(".docx")
                ) {
                    val file = File(cacheDir, logoName)
                    val outputStream = FileOutputStream(file)
                    contentResolver.openInputStream(uri)?.copyTo(outputStream)
                    outputStream.flush()
                    outputStream.close()
                    fileCall.invoke(file, uri, logoName, context.contentResolver.getType(uri))
                } else {
                    onWrongFormat()
                }
            }
        }
    }
}