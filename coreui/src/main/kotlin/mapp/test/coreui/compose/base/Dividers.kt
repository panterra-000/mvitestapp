package mapp.test.coreui.compose.base

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import mapp.test.coreui.theme.TestAppTheme

@Composable
fun DividerMin(addPadding: Boolean = false, color: Color = TestAppTheme.colors.primaryDivider) {
    Divider(
        Modifier
            .fillMaxWidth()
            .padding(top = if (addPadding) 5.dp else 0.dp)
            .height(1.dp)
            .background(color)
    )
}

@Composable
fun DividerMid(addPadding: Boolean = false, color: Color = TestAppTheme.colors.primaryDivider) {
    Divider(
        Modifier
            .fillMaxWidth()
            .height(2.dp)
            .padding(top = if (addPadding) 10.dp else 0.dp)
            .background(color)
    )
}

@Composable
fun DividerMax(addPadding: Boolean = false, color: Color = TestAppTheme.colors.primaryDivider) {
    Divider(
        Modifier
            .fillMaxWidth()
            .padding(top = if (addPadding) 10.dp else 0.dp)
            .height(4.dp)
            .background(color)
    )
}


@Composable
fun LabeledButtonHorizontalBorderLine(color: Color = TestAppTheme.colors.labeledButtonBorderTint) {
    Divider(
        Modifier
            .fillMaxWidth()
            .height(1.dp)
            .background(color)
    )
}


@Composable
fun SecondaryDividerMin(
    addPadding: Boolean = false,
    color: Color = TestAppTheme.colors.secondaryDivider
) {
    Divider(
        Modifier
            .fillMaxWidth()
            .padding(top = if (addPadding) 5.dp else 0.dp)
            .height(1.dp)
            .background(color)
    )
}

@Composable
fun BottomSheetDivider() {
    Divider(
        Modifier
            .width(64.dp)
            .height(4.dp)
            .clip(RoundedCornerShape(2.dp))
            .background(TestAppTheme.colors.bottomSheetDivider)
    )
}

@Composable
fun DividerMinWithMargin(
    horizontalMargin: Dp = 37.dp,
    verticalMargin: Dp = 0.dp,
    color: Color = TestAppTheme.colors.secondaryDivider
) {
    Divider(
        Modifier
            .fillMaxWidth()
            .padding(vertical = verticalMargin, horizontal = horizontalMargin)
            .height(1.dp)
            .background(color)
    )
}

@Composable
fun VerticalDividerMinWithMargin(
    horizontalMargin: Dp = 0.dp,
    verticalMargin: Dp = 0.dp,
    color: Color = TestAppTheme.colors.inactiveBorder
) {
    Divider(
        Modifier
            .height(32.dp)
            .width(1.dp)
            .background(color)
    )
}





