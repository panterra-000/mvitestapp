package mapp.test.coreui.compose.base.images

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter


@Composable
fun LocalImage(resId: Int, height: Dp = 32.dp, width: Dp = 24.dp) {
    Image(
        painter = painterResource(id = resId),
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .height(height)
            .width(width)
    )
}

@Composable
fun LocalImageWithPadding(resId: Int, paddingTop: Dp = 0.dp) {
    Image(
        painter = painterResource(id = resId),
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = paddingTop)
    )
}


@Composable
fun LocalImageFillMaxSize(resId: Int) {
    Image(
        painter = painterResource(id = resId),
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = Modifier.fillMaxSize()
    )
}

@Composable
fun LocalImageWithHeight(resId: Int, height: Dp = 14.dp) {
    Image(
        painter = painterResource(id = resId),
        modifier = Modifier.height(height),
        contentDescription = null,
    )
}


@Composable
fun LocalImageWithSize(resId: Int, size: Dp = 20.dp, offsetX: Dp = 0.dp, offsetY: Dp = 0.dp) {
    Image(
        painter = painterResource(id = resId),
        modifier = Modifier
            .size(size),
        contentDescription = null,
    )
}


@Composable
fun ClickableImageWithSize(resId: Int, size: Dp = 40.dp, onclick: () -> Unit) {
    Image(
        painter = painterResource(id = resId),
        modifier = Modifier
            .size(size)
            .clip(RoundedCornerShape(50))
            .clickable { onclick() },
        contentDescription = null,
    )
}