package mapp.test.coreui.compose.base.tabs

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Tab
import androidx.compose.material.TabPosition
import androidx.compose.material.TabRow
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import mapp.test.coreui.theme.TestAppTheme
import mapp.test.coreui.compose.base.texts.Text16spBoldActiveTitle
import mapp.test.coreui.compose.base.texts.Text16spBoldTitle

@Composable
fun CustomTabs(selectedPosition: (Int) -> Unit) {
    var selectedIndex by remember { mutableStateOf(0) }

    val list = listOf("Name 1", "Name 2")

    TabRow(selectedTabIndex = selectedIndex,
        backgroundColor = TestAppTheme.colors.primaryInputBackground,
        modifier = Modifier
            .clip(RoundedCornerShape(12.dp))
            .padding(1.dp),
        indicator = { tabPositions: List<TabPosition> ->
//            Box {}
        },
        divider = {

        }
    ) {
        list.forEachIndexed { index, text ->
            val selected = selectedIndex == index
            Tab(
                modifier = if (selected) Modifier
                    .clip(RoundedCornerShape(12.dp))
                    .background(
                        TestAppTheme.colors.primaryButtonBackground
                    )
                else Modifier
                    .clip(RoundedCornerShape(12.dp))
                    .background(
                        TestAppTheme.colors.primaryInputBackground
                    ),
                selected = selected,
                onClick = {
                    selectedIndex = index
                    selectedPosition(selectedIndex)
                },
                text = {
                    if (selected) Text16spBoldActiveTitle(text = text) else
                        Text16spBoldTitle(text = text)
                }
            )
        }
    }
}