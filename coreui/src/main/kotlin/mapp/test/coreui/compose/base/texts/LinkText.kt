package mapp.test.coreui.compose.base.texts

import androidx.compose.foundation.clickable
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.sp
import mapp.test.coreui.theme.TestAppTheme

@Composable
fun LinkTextView12Sp(text: String = "", onclick: () -> Unit) {
    Text(
        text = text,
        fontSize = 12.sp,
        color = TestAppTheme.colors.primaryClickableText,
        style = TextStyle(textDecoration = TextDecoration.Underline),
        modifier = Modifier.clickable(onClick = onclick)
    )
}