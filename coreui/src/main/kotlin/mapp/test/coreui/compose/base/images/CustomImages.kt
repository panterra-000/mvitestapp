package mapp.test.coreui.compose.base.images

import android.net.Uri
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import coil.compose.rememberImagePainter
import mapp.test.coreui.theme.TestAppTheme
import mapp.test.coreui.compose.base.file_picker.getFileLauncherWithCursor
import java.io.File

const val STORAGE_BASE_URL = ""

@Composable
fun SetAvatarImageView(
    fineState: MutableState<Boolean>,
    selectedFile: (File) -> Unit,
    placeHolderImageResourceId: Int,
    onWrongFormat: () -> Unit
) {

    val mFileUri = remember { mutableStateOf<Uri?>(null) }

    val pickPictureLauncher = getFileLauncherWithCursor(fileCall = { mFile, uri, name1, name2 ->
        selectedFile(mFile)
        mFileUri.value = uri
    }, onWrongFormat = {
        mFileUri.value = null
        onWrongFormat()
    }
    )

    Box(
        Modifier
            .size(110.dp)
    ) {

        val modifier = Modifier
            .align(Alignment.TopCenter)
            .size(100.dp)
            .clip(RoundedCornerShape(50))
            .background(TestAppTheme.colors.badgeAvatarBackground)

        val errorModifier = Modifier
            .align(Alignment.TopCenter)
            .size(100.dp)
            .clip(RoundedCornerShape(50))
            .border(
                border = BorderStroke(4.dp, Color.Red),
                shape = RoundedCornerShape(50)
            )
            .background(TestAppTheme.colors.badgeAvatarBackground)

        Image(
            painter = if (mFileUri.value != null) rememberImagePainter(
                data = mFileUri.value  // or ht
            ) else painterResource(id = placeHolderImageResourceId),
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = if (fineState.value) modifier else errorModifier
        )

        Image(
            painter = painterResource(id = placeHolderImageResourceId),
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .align(Alignment.BottomEnd)
                .clip(RoundedCornerShape(50))
                .clickable {
                    pickPictureLauncher.launch("image/*")
                }
        )
    }
}


@Composable
fun AvatarImageWithSetView(
    lastAvatarUrl: String = "",
    mFileUriState: MutableState<Uri?>,
    placeHolderImageResourceId: Int,
    selectedFile: (File) -> Unit
) {

    val pickPictureLauncher = getFileLauncherWithCursor(fileCall = { mFile, uri, name1, name2 ->
        selectedFile(mFile)
        mFileUriState.value = uri
    }, onWrongFormat = {

    })

    Box(
        Modifier.size(110.dp)
    ) {
        Image(
            painter = if (mFileUriState.value != null) rememberImagePainter(
                data = mFileUriState.value  // or ht
            ) else rememberAsyncImagePainter(model = STORAGE_BASE_URL + lastAvatarUrl),
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .align(Alignment.TopCenter)
                .size(100.dp)
                .clip(RoundedCornerShape(50))
                .background(TestAppTheme.colors.badgeAvatarBackground)
        )

        Image(
            painter = painterResource(id = placeHolderImageResourceId),
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .align(Alignment.BottomEnd)
                .clip(RoundedCornerShape(50))
                .clickable {
                    pickPictureLauncher.launch("image/*")
                }
        )
    }
}


@Composable
fun AvatarImageView(avatarImageUrl: String = "") {
    Box(
        Modifier
            .size(110.dp)
    ) {
        Image(
            painter = rememberAsyncImagePainter(model = STORAGE_BASE_URL + avatarImageUrl),
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .align(Alignment.TopCenter)
                .size(100.dp)
                .clip(RoundedCornerShape(50))
                .background(TestAppTheme.colors.badgeAvatarBackground)
        )
    }
}
