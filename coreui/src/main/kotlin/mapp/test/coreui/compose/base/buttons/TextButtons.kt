package mapp.test.coreui.compose.base.buttons

import androidx.compose.foundation.clickable
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import mapp.test.coreui.theme.TestAppTheme


@Composable
fun TextButton14sp(
    text: String,
    color: Color = TestAppTheme.colors.primaryClickableText,
    onclick: () -> Unit
) {
    Text(
        text = text,
        fontSize = 14.sp,
        color = color,
        fontWeight = FontWeight.Bold,
        modifier = Modifier.clickable {
            onclick()
        })
}
