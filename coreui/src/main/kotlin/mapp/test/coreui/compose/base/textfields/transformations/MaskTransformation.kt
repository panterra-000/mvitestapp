package mapp.voyo.core_ui.composable.base.textfields.transformations

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.input.OffsetMapping
import androidx.compose.ui.text.input.TransformedText
import androidx.compose.ui.text.input.VisualTransformation

class PrefixTransformation(val prefix: String) : VisualTransformation {
    override fun filter(text: AnnotatedString): TransformedText {
        return PrefixFilter(text, prefix)
    }
}

fun PrefixFilter(number: AnnotatedString, prefix: String): TransformedText {

    val out = prefix + number.text
    val prefixOffset = prefix.length

    val numberOffsetTranslator = object : OffsetMapping {
        override fun originalToTransformed(offset: Int): Int {
            return offset + prefixOffset
        }

        override fun transformedToOriginal(offset: Int): Int {
            if (offset <= prefixOffset - 1) return prefixOffset
            return offset - prefixOffset
        }
    }

    return TransformedText(AnnotatedString(out), numberOffsetTranslator)
}


class PhoneFormatTransformation : VisualTransformation {
    override fun filter(text: AnnotatedString): TransformedText {
        return mobileNumberFilter(text)
    }
}

fun mobileNumberFilter(text: AnnotatedString): TransformedText {
    val mask = "+0 (000) 000 00 00"
    // change the length
    val trimmed =
        if (text.text.length >= 11) text.text.substring(0..10) else text.text

    val annotatedString = AnnotatedString.Builder().run {
        append("+")
        for (i in trimmed.indices) {
            append(trimmed[i])
            if (i == 0) {
                append(" (")
            }
            if (i == 3) {
                append(") ")
            }
            if (i == 6 || i == 8) {
                append(" ")
            }
        }
        pushStyle(SpanStyle(color = Color.LightGray))
        append(mask.takeLast(mask.length - length))
        toAnnotatedString()
    }

    val phoneNumberOffsetTranslator = object : OffsetMapping {
        override fun originalToTransformed(offset: Int): Int {
            if (offset == 0) return offset + 1
            if (offset <= 3) return offset + 3
            if (offset <= 6) return offset + 5
            if (offset <= 8) return offset + 6
            if (offset <= 10) return offset + 7
            return 18
        }

        override fun transformedToOriginal(offset: Int): Int {
            if (offset <= 2) return offset - 1
            if (offset <= 7) return offset - 3
            if (offset <= 12) return offset - 5
            if (offset <= 15) return offset - 6
            if (offset <= 18) return offset - 7

            return 11
        }
    }

    // 1 (111) 111 11 11

    return TransformedText(annotatedString, phoneNumberOffsetTranslator)
}
