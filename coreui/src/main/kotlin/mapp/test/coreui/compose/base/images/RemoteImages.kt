package mapp.test.coreui.compose.base.images

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter


@Composable
fun RoundedImageWithUrl(
    url: String? = "https://cdn.pixabay.com/photo/2022/07/07/07/24/clouds-7306684_1280.jpg",
    size: Dp = 96.dp
) {
    Image(
        painter = rememberAsyncImagePainter(model = STORAGE_BASE_URL + url),
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .size(size)
            .clip(RoundedCornerShape(50))
    )
}

