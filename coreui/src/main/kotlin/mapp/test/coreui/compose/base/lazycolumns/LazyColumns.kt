package mapp.test.coreui.compose.base.lazycolumns

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun LazyColumnWithPadding(
    padding: Dp = 24.dp,
    content: LazyListScope.() -> Unit
) {
    LazyColumn(modifier = Modifier.padding(padding),content = {
        content()
    })
}