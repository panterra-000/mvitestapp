package mapp.test.coreui.compose.base

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import mapp.test.coreui.compose.base.Spacer24dp
import mapp.test.coreui.compose.base.columns.BorderedColumnFillMaxWidthWidthWithPadding

@Composable
fun ProgressBar() {
    Box(Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        CircularProgressIndicator()
    }
}


@Composable
fun ProgressBarWithBlock() {
    Box(
        Modifier
            .fillMaxWidth()
            .height(300.dp)
            .clickable {
            }, contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator()
    }
}


@Composable
fun ProgressBarWithBlock200Dp() {
    Spacer24dp()
    BorderedColumnFillMaxWidthWidthWithPadding {
        Box(
            Modifier
                .fillMaxWidth()
                .height(200.dp)
                .clickable {
                }, contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator()
        }
    }
}

@Composable
fun ProgressBarWithBlock100Dp() {
    Spacer24dp()
    BorderedColumnFillMaxWidthWidthWithPadding {
        Box(
            Modifier
                .fillMaxWidth()
                .height(100.dp)
                .clickable {
                }, contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator()
        }
    }
}


@Composable
fun ProgressBarWithBlock60Dp() {
    Spacer24dp()
    BorderedColumnFillMaxWidthWidthWithPadding {
        Box(
            Modifier
                .fillMaxWidth()
                .height(60.dp)
                .clickable {
                }, contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator()
        }
    }
}
