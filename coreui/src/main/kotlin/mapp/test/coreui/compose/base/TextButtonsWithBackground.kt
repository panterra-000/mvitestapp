package mapp.voyo.core_ui.composable.base

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import mapp.test.coreui.theme.TestAppTheme

@Composable
fun TextWithBackground(
    text: String,
    size: TextUnit = 12.sp,
    color: Color = TestAppTheme.colors.primaryButtonText
) {
    Text(
        text = text,
        fontSize = size,
        color = color,
        modifier = Modifier
            .clip(RoundedCornerShape(4.dp))
            .background(TestAppTheme.colors.primaryButtonBackground)
            .padding(2.dp)
    )
}