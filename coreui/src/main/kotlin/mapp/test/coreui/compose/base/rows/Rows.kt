package mapp.test.coreui.compose.base.rows

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import mapp.test.coreui.theme.TestAppTheme

@Composable
fun RowFillMaxWidth(
    backgroundColor: Color = TestAppTheme.colors.transparent,
    verticalPadding: Dp = 0.dp,
    horizontalPadding: Dp = 0.dp,
    verticalCenter: Boolean = false,
    horizontalCenter: Boolean = false,
    content: @Composable RowScope.() -> Unit
) {
    Row(
        Modifier
            .fillMaxWidth()
            .background(backgroundColor)
            .padding(vertical = verticalPadding, horizontal = horizontalPadding),
        verticalAlignment = if (verticalCenter) Alignment.CenterVertically else Alignment.Top,
        horizontalArrangement = if (horizontalCenter) Arrangement.Center else Arrangement.Start
    ) {
        content()
    }
}


@Composable
fun RowFillMaxWidthWithAlignCenter(
    horizontalMargin: Dp = 30.dp,
    content: @Composable RowScope.() -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = horizontalMargin),
        horizontalArrangement = Arrangement.Center
    ) {
        content()
    }
}


@Composable
fun RoundedCornerRowFillMaxWidth(
    backgroundColor: Color = TestAppTheme.colors.lightBackground,
    content: @Composable RowScope.() -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(12.dp))
            .background(backgroundColor)
            .padding(20.dp),
    ) {
        content()
    }
}


@Composable
fun RoundedCornerRowFillMaxWidthAlignCenter(
    backgroundColor: Color = TestAppTheme.colors.lightBackground,
    content: @Composable RowScope.() -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(12.dp))
            .background(backgroundColor)
            .padding(20.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        content()
    }
}

@Composable
fun RoundedCornerRowClickable(
    backgroundColor: Color = TestAppTheme.colors.lightBackground,
    onclick: () -> Unit,
    content: @Composable RowScope.() -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 24.dp, horizontal = 16.dp)
            .clip(RoundedCornerShape(12.dp))
            .background(backgroundColor)
            .clickable { onclick() }
            .padding(20.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        content()
    }
}
