package mapp.test.coreui.compose.base.buttons

import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource


@Composable
fun AppIconButton(resourceId: Int, onClick: () -> Unit) {
    IconButton(onClick = onClick) {
        Icon(
            painter = painterResource(id = resourceId),
            contentDescription = null
        )
    }
}
