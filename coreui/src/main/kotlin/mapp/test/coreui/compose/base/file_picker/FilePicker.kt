package mapp.test.coreui.compose.base.file_picker

import android.net.Uri
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import mapp.test.coreui.compose.base.Spacer15dp
import mapp.test.coreui.compose.base.Spacer8dp
import mapp.test.coreui.compose.base.texts.Text16Hint
import mapp.test.coreui.compose.base.texts.Text16spBoldValue
import mapp.test.coreui.theme.TestAppTheme
import java.io.File


@Composable
fun AppFilePicker(
    hintText: String = "",
    iconResId: Int,
    selectedFile: (File?) -> Unit
) {

    val mFileUri = remember { mutableStateOf<Uri?>(null) }

    val mPicker = getFileLauncherWithCursor(fileCall = { mFile, uri, name1, name2 ->
        selectedFile(mFile)
        mFileUri.value = uri
    }, onWrongFormat = {

    })

    Spacer15dp()
    Row(
        modifier = Modifier
            .clip(RoundedCornerShape(12.dp))
            .fillMaxWidth()
            .background(TestAppTheme.colors.primaryInputBackground)
            .clickable { mPicker.launch("*/*") }
            .padding(start = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        if (mFileUri.value != null) {
            Row(
                Modifier
                    .clip(RoundedCornerShape(12.dp))
                    .background(TestAppTheme.colors.selectedFileBackground)
                    .weight(1f)
                    .padding(5.dp), verticalAlignment = Alignment.CenterVertically
            ) {
                Column(Modifier.weight(1f)) {
                    Text16spBoldValue(text = mFileUri.value!!.lastPathSegment.toString())
                }
                Spacer8dp()
                // Need to set clear icon button !
            }
        } else {
            Column(Modifier.padding(vertical = 13.dp)) {
                Text16Hint(text = hintText)
            }
        }
        Spacer(modifier = Modifier.weight(0.5f))
        Column(
            modifier = Modifier
                .clip(RoundedCornerShape(topEnd = 12.dp, bottomEnd = 12.dp))
                .background(TestAppTheme.colors.primaryButtonBackground)
                .padding(16.dp)
        ) {
            Icon(
                painter = painterResource(id = iconResId),
                contentDescription = null,
                tint = TestAppTheme.colors.titleText
            )
        }

    }
}