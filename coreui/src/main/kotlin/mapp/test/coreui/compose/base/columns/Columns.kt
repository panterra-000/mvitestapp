package mapp.test.coreui.compose.base.columns

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import mapp.test.coreui.theme.TestAppTheme

@Composable
fun ColumnFillMaxSizeCenterPadding50(
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(50.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    )
    {
        content()
    }
}

@Composable
fun ColumnFillMaxSizePadding50(
    centerVertical: Boolean = false,
    centerHorizontal: Boolean = false,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(50.dp),
        verticalArrangement = if (centerVertical) Arrangement.Center else Arrangement.Top,
        horizontalAlignment = if (centerHorizontal) Alignment.CenterHorizontally else Alignment.Start
    )
    {
        content()
    }
}

@Composable
fun ColumnFillMaxSizeVerticalPadding50(
    centerVertical: Boolean = false,
    centerHorizontal: Boolean = false,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(vertical = 50.dp),
        verticalArrangement = if (centerVertical) Arrangement.Center else Arrangement.Top,
        horizontalAlignment = if (centerHorizontal) Alignment.CenterHorizontally else Alignment.Start
    )
    {
        content()
    }
}

@Composable
fun ColumnMaxSizeTopPadding50(
    centerVertical: Boolean = false,
    centerHorizontal: Boolean = false,
    backgroundColor: Color = TestAppTheme.colors.primaryBackground,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor)
            .padding(top = 50.dp),
        verticalArrangement = if (centerVertical) Arrangement.Center else Arrangement.Top,
        horizontalAlignment = if (centerHorizontal) Alignment.CenterHorizontally else Alignment.Start
    )
    {
        content()
    }
}

@Composable
fun ColumnMaxSizePadding24Dp(
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(24.dp)
            .verticalScroll(rememberScrollState())
    ) {
        content()
    }
}

@Composable
fun ColumnMaxSizeWithBackGround(
    backgroundColor: Color = TestAppTheme.colors.primaryBackground,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        content()
    }
}


@Composable
fun ScrollableColumnWithBackgroundMaxSize(
    backgroundColor: Color = TestAppTheme.colors.primaryBackground,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor)
            .verticalScroll(rememberScrollState())
            .padding(top = 42.dp, bottom = 36.dp),
    ) {
        content()
    }
}


@Composable
fun ColumnMaxSizeScrollable(
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        content()
    }
}


@Composable
fun PrimaryColumnMaxSizeScrollable(
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .padding(24.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        content()
    }
}

@Composable
fun ColumnMaxSize(
    alignCenter: Boolean = true,
    padding: Dp = 0.dp,
    content: @Composable ColumnScope.() -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(padding),
        horizontalAlignment = if (alignCenter) Alignment.CenterHorizontally else Alignment.Start
    ) {
        content()
    }
}


@Composable
fun ColumnMaxSizePWithBackground(
    alignCenter: Boolean = true,
    backgroundColor: Color = TestAppTheme.colors.sidebarBackground,
    padding: Dp = 0.dp,
    content: @Composable ColumnScope.() -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(backgroundColor)
            .padding(padding),
        horizontalAlignment = if (alignCenter) Alignment.CenterHorizontally else Alignment.Start
    ) {
        content()
    }
}

@Composable
fun ColumnMaxSizeWithBackground(
    backgroundColor: Color = TestAppTheme.colors.sidebarBackground,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor)
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        content()
    }
}

@Composable
fun ColumnWithBackgroundMaxSize(
    backgroundColor: Color = TestAppTheme.colors.sidebarBackground,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor)
            .padding(bottom = 12.dp),
    ) {
        content()
    }
}

@Composable
fun ColumnPadding24dp(
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .padding(24.dp),
    ) {
        content()
    }
}


@Composable
fun ColumnPaddingHorizontal24dp(
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .padding(horizontal = 24.dp),
    ) {
        content()
    }
}

@Composable
fun ColumnContentCenterHorizontal(
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        content()
    }
}


@Composable
fun BorderedColumnFillMaxWidthWidthWithPadding(
    borderRadius: Dp = 8.dp,
    padding: Dp = 16.dp,
    backgroundColor: Color = TestAppTheme.colors.lightBackground,
    alignCenter: Boolean = true,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 24.dp)
            .clip(RoundedCornerShape(borderRadius))
            .background(backgroundColor)
            .padding(padding),
        horizontalAlignment = if (alignCenter) Alignment.CenterHorizontally else Alignment.Start
    ) {
        content()
    }
}


@Composable
fun ColumnBodySecondaryWithBackGround(
    backgroundColor: Color = TestAppTheme.colors.secondaryBodyBackground,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        content()
    }
}

@Composable
fun ColumnFillMaxWidth(
    backgroundColor: Color = TestAppTheme.colors.transparent,
    centerHorizontal: Boolean = false,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(backgroundColor),
        horizontalAlignment = if (centerHorizontal) Alignment.CenterHorizontally else Alignment.Start
    ) {
        content()
    }
}


@Composable
fun ColumnFillMaxWidthAlignCenter(
    padding: Dp = 12.dp,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        Modifier
            .fillMaxWidth()
            .background(TestAppTheme.colors.lightBackground)
            .padding(padding),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        content()
    }
}


@Composable
fun ColumnAlignCenterStandardFillMaxWidth(
    padding: Dp = 24.dp,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        Modifier
            .fillMaxWidth()
            .padding(padding),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        content()
    }
}


@Composable
fun ColumnAlignCenterWithPadding(
    horizontalPadding: Dp = 24.dp,
    verticalPadding: Dp = 0.dp,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        Modifier
            .fillMaxWidth()
            .padding(horizontal = horizontalPadding, vertical = verticalPadding),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        content()
    }
}

