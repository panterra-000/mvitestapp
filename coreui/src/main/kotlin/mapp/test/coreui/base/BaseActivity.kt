package mapp.test.coreui.base

import android.net.ConnectivityManager
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import androidx.activity.ComponentActivity

open class BaseActivity : ComponentActivity() {
    open fun checkAndSetupLocation(): Boolean {
        return true
    }

    open fun isNetworkAvailable():Boolean{
        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)
        return (capabilities!=null&&capabilities.hasCapability(NET_CAPABILITY_INTERNET))
    }

}
