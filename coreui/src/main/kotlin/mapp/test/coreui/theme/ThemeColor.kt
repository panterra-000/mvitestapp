package mapp.test.coreui.theme

import androidx.compose.ui.graphics.Color

object ThemeColor {

    fun getColors(darkTheme: Boolean): TestAppColors {

        return if (darkTheme) {
            themeDark
        } else {
            themeLight
        }
    }

    private val themeLight = TestAppColors(
        statusBar = Color(0xFFf3f4f6),
        secondaryStatusBar = Color(0xFF1f2126),
        primaryAppbar = Color(0xFF000000),
        SecondaryAppbar = Color(0xFF000000),

        primaryBackground = Color(0xFFf3f4f6),
        sidebarBackground = Color(0xFFFFFFFF),
        secondaryBackground = Color(0xFF1f2126),
        primaryButtonBackground = Color(0xFFf5b754),
        disabledButtonBackground = Color(0xFFFADBAA),
        secondaryButtonBackground = Color(0xFF1f2126),
        variantButtonBackground = Color(0xFF1E2125),
        defaultPlasticCardBackground = Color(0xFF1f2126),
        homeTopBarBackground = Color(0xFFF7F5F6),
        homeTopBarCenterGradient = Color(0xFFF7F5F6),
        homeTopBarCenter2Gradient = Color(0xE6F7F5F6),
        homeTopBarBottomGradient = Color(0x00F7F5F6),
        orderItemBackground = Color(0xFFFFFFFF),
        acceptedDriverItemBackground = Color(0xFFFFFFFF),
        supportBodyBackground = Color(0xFFF9F9F9),
        secondaryBodyBackground = Color(0xFFF9F9F9),
        bottomSheetBackground = Color(0xFFFFFFFF),
        bottomSheetSecondaryBackground = Color(0xFFF9F9F9),
        disabledMessageBackground = Color(0xFFE92727),

        primaryInputBackground = Color(0xFFFFFFFF),
        secondaryInputBackground = Color(0xFFF9F9F9),
        mapInputBackground = Color(0xFFF9F9F9),
        mapPlaceListBackground = Color(0xFFF9F9F9),
        lightBackground = Color(0xFFFFFFFF),
        videoFrameBackground = Color(0xFF000000),
        selectedFileBackground = Color(0xFFF3F8FF),
        stopButtonBackground = Color(0xFF1E2125),
        arrivedSignalBackground = Color(0xFFF2C376),

        inactiveButtonBackground = Color(0xFF7A7A7A),
        contactPlaceHolderBackground = Color(0xFF407BFF),
        disabled = Color(0xFFC3C4C5),
        badgeAvatarBackground = Color(0xFFDBD8FC),
        switchThumbColor = Color(0xFFFFFFFF),
        checkedTrackColor = Color(0xFFf5b754),
        unCheckedTrackColor = Color(0xFFFBD9D9),

        primaryText = Color(0xFF1E2125),
        positiveText = Color(0xFF00AE26),
        titleText = Color(0xFFFFFFFF),
        primaryClickableText = Color(0xFFF5B754),
        secondaryText = Color(0xFFF5B754),
        inactiveText = Color(0xFF858F9C),
        disableText = Color(0xFFbfc4ca),
        hintText = Color(0xFF858F9C),
        nonActionText = Color(0xFF505862),
        primaryButtonText = Color(0xFFFFFFFF),
        secondaryButtonText = Color(0xFFF5B754),
        plasticCardNumberText = Color(0xFFE0E3E6),
        plasticCardTitleText = Color(0xFFE0E3E6),
        plasticCardDateText = Color(0xFF858F9C),
        plasticCardCVCText = Color(0xFF858F9C),
        stripeTitleText = Color(0xFF8792A2),
        cardItemInactiveTitleText = Color(0xFF969696),
        testResultText = Color(0xFFF0975B),
        errorText = Color(0xFFE50101),
        approvedStatusText = Color(0xFF00AE26),
        pendingStatusText = Color(0xFFF5B754),
        rejectedStatusText = Color(0xFFE50101),
        advertisingText = Color(0xFF505862),
        clearPrimaryButtonText = Color(0xFFDF2424),

        primaryDivider = Color(0xFFFFFFFF),
        secondaryDivider = Color(0xFFD9D9D9),
        bottomSheetDivider = Color(0xFFC2C7CD),
        transparent60 = Color(0x99000000),
        overlayFoggyTransparentTint = Color(0x99000000),
        transparentResult = Color(0x99FFFFFF),
        transparent = Color(0x00000000),
        labeledButtonBorderTint = Color(0xFFD9D9D9),
        finishSwipeButtonBackground = Color(0xFF271802),

        inactiveBorder = Color(0xFFE0E3E6),
        activeBorder = Color(0xFFF5B754),
        mapRouteLineTint = Color(0xFFE9950E),
        driverWaveTint = Color(0xFF76ACFF),
        riderWaveTint = Color(0xFFE9950E),
        recentLocationItemBackground = Color(0xFFFDEDD4),
        orderTimeText = Color(0xFF505862),
        durationLineTint = Color(0xFFE9950E),
        durationLineSecondTint = Color(0xFFFFD797),

        chatBackGround = Color(0xFFf9f9f9),
        chatTextFieldBackGround = Color(0xFFf9f9f9),
        chatSentMessageTextColor = Color(0xFFFFFFFF),
        chatSentMessageBackground = Color(0xFFF5B754),
        chatReceivedMessageTextColor = Color(0xFF15213B),
        chatReceivedMessageBackground = Color(0xFFFDEDD4),
        chatSentMessageTimeColor = Color(0xFFE0E3E6),
        chatReceivedMessageTimeColor = Color(0xFF858F9C),
        chatUnRideMessagesCountBackground = Color(0xFFF5B754),
    )

    private val themeDark = themeLight
}