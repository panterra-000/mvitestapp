package mapp.test.core.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

fun Date.toString(format: String, locale: Locale = Locale.getDefault()): String {
    val formatter = SimpleDateFormat(format, locale)
    return formatter.format(this)
}

fun getCurrentDateTime(): Date {
    return Calendar.getInstance().time
}

fun getCurrentTimeString(): String {
    return getCurrentDateTime().toString("yyyy/MM/dd HH:mm:ss")
}

fun getCurrentTimeIn12Hours(): String {
    return getCurrentDateTime().toString("HH:mm a")
}

@SuppressLint("SimpleDateFormat")
fun String.changeDateFormat(): String {
    val sdfSource = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    val date = sdfSource.parse(this)
    val sdfDestination = SimpleDateFormat("MM-dd-yyyy - hh:mm a")
    return sdfDestination.format(date)
}

fun getDifferentTime(oldTime: String?): Long {
    return if (oldTime != null) {
        val currentTime = getCurrentTimeWithTimeZone()
        (convertDateToLong(currentTime) - convertDateToLong(oldTime)) / 1000
    } else {
        0L
    }
}

fun convertDateToLong(date: String): Long {
    val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    return df.parse(date).time
}

fun currentTimeToLong(): Long {
    return System.currentTimeMillis()
}

fun getCurrentTimeWithTimeZone(timeZoneId: String = "UTC:00"): String {
    val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    df.timeZone = TimeZone.getTimeZone(timeZoneId)
    val date = Date()
    return df.format(date)
}

// Server Time format : 2023-07-16T17:07:57.529Z