package mapp.test.core.utils

import androidx.compose.runtime.MutableState

fun <T> MutableState<List<T>>.addNewItem(item: T) {
    val list = ArrayList<T>()
    list.addAll(this.value)
    if (!item.checkAvailability(list)) list.add(item)
    this.value = list
}


fun <T> MutableState<List<T>>.addNewItems(items: List<T>) {
    val list = ArrayList<T>()
    list.addAll(this.value)
    list.addAll(items)
    this.value = list
}

fun <T> MutableState<List<T>>.addOrRemoveItem(item: T) {
    val list = ArrayList<T>()
    list.addAll(this.value)
    if (item.checkAvailability(list)) list.remove(item)
    else list.add(item)
    this.value = list
}

fun <T> T.checkAvailability(list: List<T>): Boolean {
    var isAvailable = false
    list.forEach {
        if (this == it) {
            isAvailable = true
        }
    }
    return isAvailable
}
