package mapp.test.core.utils

import kotlinx.coroutines.*

/**
 * Created by Davronbek Raximjanov on 04-Nov-20
 */

object Coroutines {
    fun <T : Any> ioThenMain(work: suspend (() -> T?), callback: ((T?) -> Unit)) =
        CoroutineScope(Dispatchers.Main).launch {
            val data = CoroutineScope(Dispatchers.IO).async rt@{
                return@rt work()
            }.await()
            callback(data)
        }

    fun <T : Any> ioBasic(work: suspend (() -> T?)) =
        CoroutineScope(Dispatchers.IO).launch {
            work()
        }

    fun <T : Any> mainBasic(work: suspend (() -> T?)) =
        CoroutineScope(Dispatchers.Main).launch {
            work()
        }

    fun <T : Any> dispatcherThenMain(work: suspend (() -> T?), callback: ((T?) -> Unit)) =
        CoroutineScope(Dispatchers.Main).launch {
            val data = CoroutineScope(Dispatchers.Default).async rt@{
                return@rt work()
            }.await()
            callback(data)
        }
}

suspend fun delayBySecond(second: Long = 1L) {
    delay(second * 1000)
}