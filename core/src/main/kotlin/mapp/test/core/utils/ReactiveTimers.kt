package mapp.test.core.utils

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

fun ViewModel.downTime60(timeViewState: MutableState<String>, overTime: () -> Unit) {
    val timeCountState = mutableStateOf(60)
    viewModelScope.launch {
        while (timeCountState.value > 0) {
            delay(1000)
            timeCountState.value--
            if (timeCountState.value < 10) {
                timeViewState.value = "00:0${timeCountState.value}"
            } else {
                timeViewState.value = "00:${timeCountState.value}"
            }
        }
        overTime()
    }
}

fun timer(timeCountState: MutableState<Long>, timeOver: Boolean) {
    if (timeOver) {
        Coroutines.mainBasic {
            while (timeOver) {
                delayBySecond(1)
                timeCountState.value++
            }
        }
    }
}

fun downTimerWithSecond(second: Long, timeOver: () -> Unit) {
    var s = 0
    Coroutines.mainBasic {
        while (s <= second) {
            delayBySecond(1)
            s++
        }
        timeOver()
        return@mainBasic
    }
}