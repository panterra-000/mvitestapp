package mapp.test.core.utils

import java.util.concurrent.TimeUnit
import kotlin.math.roundToLong

fun Long.convertClockFormat(): String {
    val minutes = TimeUnit.SECONDS.toMinutes(this)
    val hours = TimeUnit.SECONDS.toHours(this)
    val days = TimeUnit.SECONDS.toDays(this)
    return if (hours == 0L) {
        "$minutes mins"
    } else {
        val minInHours = hours * 60
        if (days == 0L) {
            "$hours hours, ${minutes - minInHours}mins"
        } else {
            val hoursInDay = days * 24
            "$days days, ${hours - hoursInDay}hours, ${minutes - minInHours}mins"
        }
    }
}


fun Long.convertSecondTimerFormat(): String {
    val hours = this / 3600;
    val minutes = (this % 3600) / 60;
    val seconds = this % 60;

    return if (hours > 0) "$hours:$minutes:$seconds" else {
        "${minutes.toDecimalFormat()}:${seconds.toDecimalFormat()}"
    }
}


fun Long.timeShiftFormat(): String {
    val minutes = TimeUnit.SECONDS.toMinutes(this)
    val hours = TimeUnit.SECONDS.toHours(this)
    val days = TimeUnit.SECONDS.toDays(this)

    val residualSeconds = this - minutes * 60
    val residualMinutes = minutes - hours * 60
    val hoursStr = if (hours > 0) {
        hours.toDecimalFormat() + ":"
    } else {
        ""
    }
    return "$hoursStr${residualMinutes.toDecimalFormat()}:${residualSeconds.toDecimalFormat()}"

}

fun Long.toDecimalFormat(): String {
    return if (this < 10) {
        "0$this"
    } else {
        "$this"
    }
}


// input meters:Long
fun Double.roundToKilometers(): String {
    val km = this / 1000
    return "${km.roundToLong()} km"
}


// input meters:Long
fun Long.roundToKilometers(): String {
    val km = this / 1000.0
    return "${km.roundToLong()} km"
}

fun findPercent(num1: Double, num2: Double): Double {
    val a = ((num1 / num2) * 100).toInt()
    return (a.toDouble() / 100)
}

fun Double.centToDollar() = this/100
fun Float.centToDollar() = this/100
fun Int.centToDollar() = this/100