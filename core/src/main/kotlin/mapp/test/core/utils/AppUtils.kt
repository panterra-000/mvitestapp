package mapp.test.core.utils

import android.util.Log

fun myLog(message: String, tag: String = "TAG_97") = Log.d(tag, message)