package mapp.voyo.core.util.intents

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast

fun phoneCallIntent(context: Context, phoneNumber: String) {
    val u = Uri.parse("tel:$phoneNumber")
    val i = Intent(Intent.ACTION_DIAL, u)
    try {
        context.startActivity(i)
    } catch (s: SecurityException) {
        Toast.makeText(context, "An error occurred", Toast.LENGTH_SHORT).show()
    }
}