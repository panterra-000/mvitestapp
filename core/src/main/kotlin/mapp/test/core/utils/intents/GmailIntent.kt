package mapp.voyo.core.util.intents

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast

fun gmailIntent(context: Context) {
    val emailIntent = Intent().apply {
        action = Intent.ACTION_SEND
        data = Uri.parse("mailto:")
        type = "text/plain"
        putExtra(Intent.EXTRA_EMAIL, arrayOf("test@gmail.com"))
        putExtra(Intent.EXTRA_SUBJECT, "request wallpaper")
        putExtra(Intent.EXTRA_TEXT, "request wallpaper")
    }
    if (emailIntent.resolveActivity(context.packageManager) != null) {
        emailIntent.setPackage("com.google.android.gm")
        context.startActivity(emailIntent)
    } else {
        Toast.makeText(
            context,
            "No app available to send email!!",
            Toast.LENGTH_SHORT
        ).show()
    }
}