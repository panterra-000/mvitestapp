package mapp.test.core.utils

import android.text.TextUtils
import android.util.Patterns
import java.util.regex.Matcher
import java.util.regex.Pattern

fun testUtil() {
    println("Test String Util Function")
}

const val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$"

const val LENGTH_PATTERN = "^(?=\\S+\$).{8,}$"
const val DIGIT_PATTERN = "^(?=.*[0-9])(?=\\S+\$).{1,}$"
const val LOWERCASE_LETTER_PATTERN = "^(?=.*[a-z])(?=\\S+\$).{1,}$"
const val UPPERCASE_LETTER_PATTERN = "^(?=.*[A-Z])(?=\\S+\$).{1,}$"
const val SPECIAL_CHAR_PATTERN = "^(?=.*[@#\$%^&+=])(?=\\S+\$).{1,}$"


fun isValidPassword(password: String?): String {
    val patternLength: Pattern = Pattern.compile(LENGTH_PATTERN)
    val patternDigit: Pattern = Pattern.compile(DIGIT_PATTERN)
    val patternLowerCase: Pattern = Pattern.compile(LOWERCASE_LETTER_PATTERN)
    val patternUpperCase: Pattern = Pattern.compile(UPPERCASE_LETTER_PATTERN)
    val patternSpecialChar: Pattern = Pattern.compile(SPECIAL_CHAR_PATTERN)

    return when {
        !patternLength.matcher(password).matches() -> {
            "Must be at least 8 characters!"
        }
        !patternDigit.matcher(password).matches() -> {
            "Min one digit is required!"
        }
        !patternLowerCase.matcher(password).matches() -> {
            "Min one lowercase letter is required!"
        }
        !patternUpperCase.matcher(password).matches() -> {
            "Min one uppercase letter required!"
        }
        !patternSpecialChar.matcher(password).matches() -> {
            "Min one special character is required!"
        }
        else -> ""
    }
}


fun isSimpleValidPassword(password: String?): Boolean {
    val pattern: Pattern = Pattern.compile(PASSWORD_PATTERN)
    val matcher: Matcher = pattern.matcher(password)
    return matcher.matches()
}

fun isValidEmail(email: String): Boolean {
    return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
}

fun main() {
}