package mapp.test.core.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

inline fun ViewModel.launchIOScope(
    crossinline body:suspend CoroutineScope.()->Unit
){
    viewModelScope.launch(Dispatchers.IO) {
        body()
    }
}


inline fun ViewModel.launchMainScope(
    crossinline body:suspend CoroutineScope.()->Unit
){
    viewModelScope.launch(Dispatchers.Main) {
        body()
    }
}

inline fun ViewModel.launchDefaultScope(
    crossinline body:suspend CoroutineScope.()->Unit
){
    viewModelScope.launch {
        body()
    }
}
